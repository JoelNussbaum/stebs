﻿module Stebs {

    export var ui = {
        editorContentChanged: false,

        architecturePixelWidth: 0,

        /**
         * Sets the flag, if stebs thinks the editor content is changed.
         */
        setEditorContentChanged(value: boolean) {
            ui.editorContentChanged = value;
            $('#filename-star').css('display', value ? 'inline' : 'none');
        },

        /**
         * Returns if the editor content is flaged as changed.
         */
        isEditorContentChanged(): boolean {
            return ui.editorContentChanged;
        },

        /**
         * Sets the width of #codingView to a prozentual value.
         * This allows correct browser resizing without additional client code.
         */
        setCodingViewWidth(): void {
            ui.fixSizingErrors();
            var width = (visible.architecture ? ' - ' + Widths.architecture : '');
            $('#codingView').css('width', 'calc(100% - ' + Widths.sidebar + ' ' + width + ')');
        },

        /**
        * Sets the width of #simulation to a prozentual value.
        * This allows correct browser resizing without additional client code.
        */
        setSimulationViewWidth(): void {
            ui.fixSizingErrors();
            var width = (visible.devices ? ' - ' + Widths.devices : '');
            $('#simulation').css('width', 'calc(100%' + width + ')');
        },

        /**
         * Opens/Closes the devices sidebar.
         */
        toggleDevices(): void {
            var animation = { left: (visible.devices ? '-=' : '+=') + Widths.devices };
            $('#devices').animate(animation);
            var animation2 = { left: animation.left, width: (visible.devices ? '+=' : '-=') + Widths.devices };
            $('#simulation').animate(animation2, ui.setSimulationViewWidth);
            visible.devices = !visible.devices;
        },

        /**
         * Opens/Closes the architecture sidebar.
         */
        toggleArchitecture(): void {
            var animation = { left: (visible.architecture ? '-=' : '+=') + Widths.architecture };
            $('#architecture').animate(animation);
            var animation2 = {
                left: animation.left,
                width: (visible.architecture ? '+=' : '-=') +
                Widths
                    .architecture
            };
            $('#codingView').animate(animation2, () => {
                if (parseInt(Widths.architecture) > Stebs.ui.getMaxArchitectureWidth()) Stebs.ui.setArchitectureWidth(Stebs.ui.getMaxArchitectureWidth());
                ui.setCodingViewWidth();
            });
            visible.architecture = !visible.architecture;
        },

        /**
         * Sets the width of #codingFrame to a prozentual value.
         * This allows correct browser resizing without additional client code.
         */
        setCodingFrameHeight(): void {
            var height = (visible.output ? ' - ' + Heights.output : '') +
                (visible.runAndDebug ? ' - ' + Heights.runAndDebug : '');
            $('#codingFrame').css('height', 'calc(100% - ' + Heights.bars + height + ')');
        },

        /**
         * Prevents the default action when dragbar is clicked and creates mouse move handler
         * @param e 
         */
        architectureDragbarMouseDown(e: JQueryMouseEventObject): void {
            e.preventDefault();
            $(document).mousemove(Stebs.ui.architectureDragbarMouseMove);
        },

        /**
         * Mouse move handler which is created after architectureDragbarMouseDown occured.
         * @param e
         */
        architectureDragbarMouseMove(e: JQueryMouseEventObject): void {
            var devicesWidth: number = parseInt(visible.devices ? Widths.devices : "0px");
            var newArchitectureWidth: number = e.pageX + 2 - devicesWidth;
            if (newArchitectureWidth >
                Stebs.ui.getMaxArchitectureWidth())
                newArchitectureWidth = Stebs.ui.getMaxArchitectureWidth();
            if (newArchitectureWidth < parseInt(Widths.sidebar) + 2) newArchitectureWidth = parseInt(Widths.sidebar) + 2;
            Stebs.ui.setArchitectureWidth(newArchitectureWidth);
            ui.fixSizingErrors();
        },

        /**
         * Sets the architecture to a new width. Works only if architecture is visible.
         * @param newArchitectureWidth
         */
        setArchitectureWidth(newArchitectureWidth: number) {
            if (visible.architecture) {
                var codingViewLeft: number = newArchitectureWidth + parseInt(Widths.sidebar);
                $("#architecture").css({ width: (newArchitectureWidth - parseInt(Widths.sidebar)) + 'px' });

                //TODO: correct calc for the container
                $(".architecture-container").css({ width: (newArchitectureWidth - parseInt(Widths.sidebar)) + 'px' });

                Widths.architecture = String(newArchitectureWidth) + 'px';
                $("#codingView")
                    .css({
                        left: codingViewLeft,
                        width: 'calc(100% - ' + Widths.architecture + ' - ' + Widths.sidebar + ')'
                    });
                if (newArchitectureWidth > parseInt(Widths.mincanvas))
                    Stebs.architectureCanvas.setCanvasSize(newArchitectureWidth);
            }

        },

        /**
         * Sets the architecture Size to max size on resize event.
         * This is also called on normal resizes within the page because a div can't have a resize listener.
         */
        fixSizingErrors() {
            var newArchitectureWidth: number = parseInt(Widths.architecture);
            if (newArchitectureWidth > Stebs.ui.getMaxArchitectureWidth() && visible.architecture) {
                newArchitectureWidth = Stebs.ui.getMaxArchitectureWidth();
                Stebs.ui.setArchitectureWidth(newArchitectureWidth);
            }
            var mpmTitle = $(architectureCanvas.getMpmDynTable().getTableContainerElement()).children('.statictitle');
            var opdTitle = $(architectureCanvas.getOpdDynTable().getTableContainerElement()).children('.statictitle');

            mpmTitle.width($(architectureCanvas.getMpmDynTable().getTableElement()).width()); // Does not work if table is hidden while resizing.
            opdTitle.width($(architectureCanvas.getOpdDynTable().getTableElement()).width());
            if ($('#codingTopBar').width() < parseInt(Stebs.Widths.coding) + $('.accountLinks').width()) $('.accountLinks').css('display', 'none');
            else if ($('.accountLinks').css('display') === 'none') $('.accountLinks').css('display', 'initial');
            Stebs.codeEditor.refresh();
            Stebs.outputView.refresh();
        },

        /**
         * Get the max. architecture width.
         */
        getMaxArchitectureWidth(): number {
            var width = $(document).width() -
                parseInt(Widths.coding) -
                parseInt(Widths.sidebar) -
                parseInt(Widths.devices);
            if (width < parseInt(Widths.sidebar) + 2) width = parseInt(Widths.sidebar) + 2; // Minimum width is sidebar width and 2px for resizebar
            return width;
        },

        /**
        * Prevents the default action when dragbar is clicked and creates mouse move handler
        * @param e 
        */
        outputDragbarMouseDown(e: JQueryMouseEventObject): void {
            e.preventDefault();
            $(document).mousemove(Stebs.ui.outputDragbarMouseMove);
        },

        /**
         * Mouse move handler which is created after outputDragbarMouseDown occured.
         * @param e
         */
        outputDragbarMouseMove(e: JQueryMouseEventObject): void {
            var simulationOffset = (visible.runAndDebug
                ? parseInt(Heights.runAndDebug)
                : 0);

            var outputHeight: number = $('.output').parent().height() - e.pageY + 2;
            if (outputHeight >
                $('.output').parent().height() -
                parseInt(Heights.runAndDebug) -
                parseInt(Heights.bars))
                outputHeight = $('.output').parent().height() -
                    parseInt(Heights.runAndDebug) -
                    parseInt(Heights.bars);
            if (outputHeight - simulationOffset < 3) outputHeight = simulationOffset + 3; // Min height of 3px so the resizebar is still visible
            Heights.output = String(outputHeight - simulationOffset) + 'px';
            $('#codingFrame').css('height', 'calc(100% - ' + Heights.bars + ' - ' + outputHeight + 'px' + ')');

            $('.output').css('height', outputHeight - simulationOffset);
            outputView.refresh();
        },

        /**
         * Unbinds mouse move handler
         * @param e
         */
        documentMouseUp(e: JQueryMouseEventObject): void {
            $(document).unbind('mousemove');
        },

        /**
         * Opens/Closes the output bar.
         */
        toggleOutput(): void {
            outputView.refresh();
            $('#codingFrame')
                .animate({ height: (visible.output ? '+=' : '-=') + Heights.output }, ui.setCodingFrameHeight);
            visible.output = !visible.output;
            if (visible.output) {
                $('.output-container').slideDown(() => outputView.refresh());
                $('#openOutput .arrow-icon').removeClass('up').addClass('down');
                ui.repositionSimulationControl(false);
            } else {
                $('.output-container').slideUp(() => outputView.refresh());
                $('#openOutput .arrow-icon').addClass('up').removeClass('down');
                ui.repositionSimulationControl(true);
            }
        },

        /**
         * Reposition Simulation control after output is moved
         * @param complete is it shown completely or partially hidden?
         */
        repositionSimulationControl(complete: boolean) {
            var animation = { top: complete ? '-25px' : '-50px' };
            $('#run-open-link').animate(animation, { duration: 400, queue: false });
            var animation2: any;
            if (complete) {
                animation2 = { width: '170px' };
                $('#run-open-link').animate(animation2, 300);
                $('#run-open-link').unbind('mouseenter mouseleave');
            }
            else {
                animation2 = { width: '25px' };
                $('#run-open-link').animate(animation2, 300);
                var onhover = { width: '170px' };
                var offhover = { width: '25px' };

                $('#run-open-link').hover(() => {
                    $('#run-open-link').animate(onhover, { duration: 400, queue: false });
                }, () => {
                    $('#run-open-link').animate(offhover, { duration: 400, queue: false });
                });
            }
        },

        openOutput(): void {
            if (!visible.output) {
                ui.toggleOutput();
            }
        },

        showOutput(text: string): void {
            outputView.getDoc().setValue(text);
        },

        /**
        * Sets the height of #architecture to a prozentual value.
        * This allows correct browser resizing without additional client code.
        */
        setArchitectureHeight(): void {
            var height = (visible.runAndDebug ? ' - ' + Heights.runAndDebug : '');
            $('#architecture').css('height', 'calc(100%' + height + ')');
        },

        /**
         * Toggles the visibility of the run and debug panel.
         */
        toggleRunAndDebug(): void {
            $('#codingFrame')
                .animate({ height: (visible.runAndDebug ? '+=' : '-=') + Heights.runAndDebug },
                ui
                    .setCodingFrameHeight);
            $('.runAndDebug').animate({ top: (!visible.runAndDebug ? '-=' : '+=') + Heights.runAndDebug });
            $('#architecture')
                .animate({ height: (visible.runAndDebug ? '+=' : '-=') + Heights.runAndDebug },
                ui.setArchitectureHeight);
            visible.runAndDebug = !visible.runAndDebug;
            if (visible.runAndDebug) {
                $('#run-open-link .arrow-icon').removeClass('up').addClass('down');
            } else {
                $('#run-open-link .arrow-icon').addClass('up').removeClass('down');
            }
        },

        /**
         * Opens Run and debug panel.
         */
        openRunAndDebug(): void {
            if (!visible.runAndDebug) {
                ui.toggleRunAndDebug();
            }
        },

        /**
        * Highlight the given line
        */
        highlightLine(ipNr: number): void {
            var linenr = Stebs.ramContent.getLineNr(ipNr);
            Stebs.codeEditor.getDoc().setCursor({ ch: 0, line: linenr });
        },

        /**
        * Reads the selected step size from the radio buttons.
        */
        getStepSize(): SimulationStepSize {
            if ($('#instructionStepSpeed').prop('checked')) {
                return SimulationStepSize.Instruction;
            } else if ($('#macroStepSpeed').prop('checked')) {
                return SimulationStepSize.Macro;
            } else {
                return SimulationStepSize.Micro;
            }
        },

        /**
         * Sets the ram download link using the new processor id.
         */
        setRamDownloadLink(processorId: string) {
            var link = $('#downloadRam');
            var href: string = link.prop('href');
            var position = href.indexOf('?processorId');
            if (position >= 0) {
                href = href.substring(0, position);
            }
            link.prop('href', href + '?processorId=' + processorId);
        },

        /**
         * Opens Architecture in a new Window. This copies several elements from the Index html to the new Window.
         */
        openNewArchitecture() {
            //Check if browser is Internet Explorer
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
            {
                alert("Diese Funktion wird auf diesem Browser leider nicht unterstützt.");
            }

            else {
                if (architectureWindow && architectureWindow.closed === false) {
                    architectureWindow.focus();
                    return;
                }
                if (Stebs.currentSpeed < SpeedSlider.architectureHide) {
                    alert('Unable to open an architecture tab at the current Speed'); //TODO: make it possible
                    return;
                }
                var windowWidth: number = screen.width * 0.7;
                var windowSize: string = "width=" + (windowWidth) + ",height=" + (windowWidth * 2 / 3) + ",enableviewportscale=yes,location=no";
                var path: string = document.location.protocol +
                    '//' +
                    document.location.hostname +
                    ':' +
                    document.location.port + '/';

                Stebs.architectureWindow = window.open("", "", windowSize);
                $(architectureWindow.document.head).append('<title>Stebs - Architecture</title>');
                $(architectureWindow.document.head).append('<link rel="stylesheet" type= "text/css" href= "' + path + 'Content/stebs.css">');
                $(architectureWindow.document.head).append('<link rel="stylesheet" type= "text/css" href= "' + path + 'Content/normalize.css">');
                $(architectureWindow.document.body).append('<div id=canvasdiv>');
                $(architectureWindow.document.body).append('<p id="speedLimitReached" > Speed limit reached, please lower the speed if you would like to see the architecture.');

                $(architectureWindow.document.getElementById("canvasdiv")).append('<canvas id=canvas></canvas>');
                $(architectureWindow.document.getElementById("canvasdiv")).append('<div id=canvasMpm>');
                $(architectureWindow.document.getElementById("canvasdiv")).append('<div id=canvasOpD>');
                $(architectureWindow.document.getElementById("canvasdiv")).append('<div id=staticMpmTitle>');
                $(architectureWindow.document.getElementById("canvasdiv")).append('<div id=staticOpdTitle>');
                var newCanvas: HTMLElement = architectureWindow.document.getElementById("canvas");
                var canvasMpm: HTMLElement = architectureWindow.document.getElementById("canvasMpm");
                var canvasOpD: HTMLElement = architectureWindow.document.getElementById("canvasOpD");
                var staticOpdTitle: HTMLElement = architectureWindow.document.getElementById("staticOpdTitle");
                var staticMpmTitle: HTMLElement = architectureWindow.document.getElementById("staticMpmTitle");

                var canvasMpmHtml = $('#canvasMpm').html();
                var canvasOpDHtml = $('#canvasOpD').html();
                var staticOpdTitleHtml = $('#staticOpdTitle').html();
                var staticMpmTitleHtml = $('#staticMpmTitle').html();

                $(canvasMpm).html(canvasMpmHtml);
                $(canvasOpD).html(canvasOpDHtml);
                $(staticOpdTitle).html(staticOpdTitleHtml);
                $(staticMpmTitle).html(staticMpmTitleHtml);

                $(newCanvas).css({
                    width: Widths.canvas, height: Heights.canvas
                });

                Stebs.architectureWindowCanvas = new ArchitectureCanvas(<HTMLCanvasElement>newCanvas, <HTMLTableElement>architectureWindow.document.getElementById("canvasMpmTable"), <HTMLTableElement>architectureWindow.document.getElementById("canvasOpDTable"));
                Stebs.architectureWindowCanvas.setupCanvas();
                Stebs.architectureWindowCanvas.initAndUpdateCanvas();
                var firstCellArray: number[] = [];
                $(canvasMpm).find('tr:not(.tabletitle) td:first-child').each((i, e) => { firstCellArray.push(parseInt(e.textContent, 16)) });
                Stebs.architectureWindowCanvas.getMpmDynTable().init(firstCellArray, 30);
                firstCellArray = [];
                $(canvasOpD).find('tr:not(.tabletitle) td:first-child').each((i, e) => { firstCellArray.push(parseInt(e.textContent, 16)) });
                Stebs.architectureWindowCanvas.getOpdDynTable().init(firstCellArray, 30);

                //TODO: find a way to do this after the page is rendered
                setTimeout(() => { // For some reason(most likely the SOP) load or ready do not work. We need a function to be executed after the table is rendered, and that doesn't seem to happen.
                    ui.resizeArchitectureWindow();
                }, 300); // If slower PCs have a problem showing the table title correctly, higher this number.

                architectureWindow.document.close();
                architectureWindow.onresize = Stebs.ui.resizeArchitectureWindow;

                architectureWindow.onbeforeunload = (ev) => {
                    Stebs.architectureWindowCanvas = null;
                }
            }
        },

        /**
         * Resizes the canvas in the architecture tab.
         */
        resizeArchitectureWindow() {
            var width = $(architectureWindow).width();
            var height = $(architectureWindow).height();
            if ((width < Widths.architectureWindowMin) || (height < Heights.architectureWindowMin)) {
                width = Widths.architectureWindowMin;
                height = Heights.architectureWindowMin;
            }
            if (width < height * 3 / 2)
                Stebs.architectureWindowCanvas.setCanvasSize(width + parseInt(Widths.sidebar) + 5);
            else Stebs.architectureWindowCanvas.setCanvasSize(height * 3 / 2 + parseInt(Widths.sidebar) + 5);
        },


        /**
         * Allows the use of a logarithmic slider.
         */
        logarithmicScale(value: number, min: number, max: number, inverse: boolean) {
            //position will be between 0 and 100 (fixed)
            var maxPosition = 100;
            //The result should be between given min an max
            var minValue = Math.log(min);
            var maxValue = Math.log(max);
            //calculate adjustment factor
            var scale = (maxValue - minValue) / (maxPosition);
            if (inverse) {
                return (Math.log(value) - minValue) / scale;
            } else {
                return Math.exp(minValue + scale * (value));
            }
        },

        /**
         * Returns the position of the slider [0, 100] calculated using a logarithmic function.
         */
        logarithmicPosition(value: number, min: number, max: number) {
            return ui.logarithmicScale(value, min, max, true);
        },

        /**
         * Returns the value of a slider [0, 100] scaled to a logarithmic function.
         */
        logarithmicValue(position: number, min: number, max: number): number {
            return ui.logarithmicScale(position, min, max, false);
        },

        /**
         * Is Called, when the value of the speed slider changed.
         */
        speedSliderChanged() {
            var newValue = ui.logarithmicValue(parseInt($('#speedSlider').val()), SpeedSlider.max, SpeedSlider.min);
            Stebs.state.changeSpeed(newValue);
        },

    };
}
